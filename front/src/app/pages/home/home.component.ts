import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActorsService} from '../../services/actors.service';
import {takeUntil, tap} from 'rxjs/operators';
import {MoviesService} from '../../services/movies.service';
import {combineLatest, Subject} from 'rxjs';
import {MovieModel} from '../../models/movie.model';
import {ActorModel} from '../../models/actor.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  numberOfActors: number;
  numberOfMovies: number;

  constructor(
    private actorsService: ActorsService,
    private moviesService: MoviesService,
  ) {
  }

  ngOnInit(): void {
    this.getActorsAndMovies();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getActorsAndMovies(): void {
    combineLatest([
      this.actorsService.getAllActors$(),
      this.moviesService.getAllMovies$(),
    ])
      .pipe(
        takeUntil(this.destroy$),
        tap(([actors, movies]: [ActorModel[], MovieModel[]]) => {
          this.numberOfActors = actors.length;
          this.numberOfMovies = movies.length;
        }),
      )
      .subscribe()
  }

}
