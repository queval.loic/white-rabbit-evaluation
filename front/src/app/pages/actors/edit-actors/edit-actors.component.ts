import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActorsService} from '../../../services/actors.service';
import {ActorModel} from '../../../models/actor.model';
import {ActivatedRoute, Router} from '@angular/router';
import {map, switchMap, tap} from 'rxjs/operators';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-create-actors',
  templateUrl: './edit-actors.component.html',
  styleUrls: ['./edit-actors.component.scss']
})
export class EditActorsComponent implements OnInit {

  actorsForm: FormGroup;
  actor: ActorModel;

  constructor(
    private fb: FormBuilder,
    private actorsService: ActorsService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this._initForm();
    this.route.params
      .pipe(
        map(params => params.id),
        switchMap((id: string) => this.actorsService.getActorById$(id)),
        tap(actor => this.actor = actor),
        tap(actor => this._initForm(actor)),
      )
      .subscribe()
  }

  submitForm(data: ActorModel) {
    if (this.actor) {
      this.putActor(data);
    } else
      this.postActor(data);
  }

  postActor(data: ActorModel) {
    this.actorsService.postActor$(data)
      .pipe(
        tap(() => this.message.info('L\'acteur a bien été créé')),
        tap(() => this.router.navigate(['actors'])),
      )
      .subscribe()
  }

  putActor(data: ActorModel) {
    this.actorsService.putActor$(data, this.actor.id)
      .pipe(
        tap(() => this.message.info('L\'acteur a bien été modifié')),
        tap(() => this.router.navigate(['actors'])),
      )
      .subscribe()
  }

  private _initForm(actor?: ActorModel) {
    this.actorsForm = this.fb.group({
      name: [actor ? actor.name : null, [Validators.required]],
    })
  }
}
