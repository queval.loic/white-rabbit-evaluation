import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EditActorsComponent} from './edit-actors.component';

const routes: Routes = [
  {
    path: '',
    component: EditActorsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class EditActorsRoutingModule {
}
