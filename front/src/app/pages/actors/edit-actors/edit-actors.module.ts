import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditActorsComponent } from './edit-actors.component';
import {EditActorsRoutingModule} from './edit-actors.routing.module';
import {NzFormModule, NzGridModule, NzInputModule} from 'ng-zorro-antd';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [EditActorsComponent],
  imports: [
    CommonModule,
    EditActorsRoutingModule,
    NzGridModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
  ]
})
export class EditActorsModule { }
