import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActorsListComponent } from './actors-list.component';
import {ActorsListRoutingModule} from './actors-list.routing.module';
import {NzDividerModule, NzGridModule, NzTableModule} from 'ng-zorro-antd';



@NgModule({
  declarations: [ActorsListComponent],
  imports: [
    CommonModule,
    ActorsListRoutingModule,
    NzTableModule,
    NzDividerModule,
    NzGridModule,
  ]
})
export class ActorsListModule { }
