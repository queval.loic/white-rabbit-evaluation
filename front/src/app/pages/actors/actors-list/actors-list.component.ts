import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActorsService} from '../../../services/actors.service';
import {takeUntil, tap} from 'rxjs/operators';
import {ActorModel} from '../../../models/actor.model';
import {Subject} from 'rxjs';
import {NzMessageService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

@Component({
  selector: 'app-actors-list',
  templateUrl: './actors-list.component.html',
  styleUrls: ['./actors-list.component.scss']
})
export class ActorsListComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  actors: ActorModel[];

  constructor(
    private actorsService: ActorsService,
    private message: NzMessageService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.getAllActors();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getAllActors(): void {
    this.actorsService.getAllActors$()
      .pipe(
        takeUntil(this.destroy$),
        tap((actors: ActorModel[]) => this.actors = actors),
      )
      .subscribe()
  }

  deleteActor(id: number): void {
    this.actorsService.deleteActorById$(id)
      .pipe(
        tap(() => this.message.info('Cet acteur a bien été surprimé, rechargez la page pour mettre à jour la liste')),
      )
      .subscribe()
  }

  goToEditActors(id: number) {
    return this.router.navigate(['actors/' + id])
  }

}
