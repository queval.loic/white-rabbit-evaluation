import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MoviesListComponent} from './movies-list.component';
import {MoviesListRoutingModule} from './movies-list.routing.module';
import {NzDividerModule, NzGridModule, NzTableModule} from 'ng-zorro-antd';

@NgModule({
  declarations: [MoviesListComponent],
  imports: [
    CommonModule,
    MoviesListRoutingModule,
    NzGridModule,
    NzTableModule,
    NzDividerModule,
  ]
})
export class MoviesListModule {
}
