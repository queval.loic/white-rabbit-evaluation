import {Component, OnInit} from '@angular/core';
import {MoviesService} from '../../../services/movies.service';
import {takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MovieModel} from '../../../models/movie.model';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {

  destroy$: Subject<boolean> = new Subject<boolean>();
  movies: MovieModel[];

  constructor(
    private moviesServices: MoviesService,
  ) {
  }

  ngOnInit(): void {
    this.getAllMovies();
  }

  getAllMovies(): void {
    this.moviesServices.getAllMovies$()
      .pipe(
        takeUntil(this.destroy$),
        tap((movies: MovieModel[]) => this.movies = movies),
      )
      .subscribe()
  }

}
