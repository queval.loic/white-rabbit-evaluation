import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MovieModel} from '../../../models/movie.model';
import {MoviesService} from '../../../services/movies.service';
import {NzMessageService} from 'ng-zorro-antd';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-movies',
  templateUrl: './create-movies.component.html',
  styleUrls: ['./create-movies.component.scss']
})
export class CreateMoviesComponent implements OnInit {

  moviesForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private moviesService: MoviesService,
    private message: NzMessageService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this._initForm()
  }

  submitForm(data: MovieModel) {
    this.moviesService.postMovie$(data)
      .pipe(
        tap(() => this.message.info('Le film a bien été créé')),
        tap(() => this.router.navigate(['movies'])),
      )
      .subscribe()
  }

  private _initForm(): void {
    this.moviesForm = this.fb.group({
      title: [null, [Validators.required]],
      category: [null, [Validators.required]],
      releaseYear: [null, [Validators.required]],
      directors: [null, [Validators.required]],
      synopsis: [null, [Validators.required]],
      rate: [null, [Validators.required]],
      price: [null, [Validators.required]],
    })
  }
}
