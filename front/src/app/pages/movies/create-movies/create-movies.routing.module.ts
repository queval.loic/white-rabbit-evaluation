import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateMoviesComponent} from './create-movies.component';

const routes: Routes = [
  {
    path: '',
    component: CreateMoviesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CreateMoviesRoutingModule {
}
