import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateMoviesComponent} from './create-movies.component';
import {CreateMoviesRoutingModule} from './create-movies.routing.module';
import {NzFormModule, NzGridModule, NzInputModule} from 'ng-zorro-antd';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [CreateMoviesComponent],
  imports: [
    CommonModule,
    CreateMoviesRoutingModule,
    NzGridModule,
    NzFormModule,
    NzInputModule,
    ReactiveFormsModule,
  ]
})
export class CreateMoviesModule {
}
