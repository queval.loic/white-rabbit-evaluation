import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NavigationComponent} from './navigation.component';

const routes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'actors',
        loadChildren: () => import('../actors/actors-list/actors-list.module').then(m => m.ActorsListModule),
      },
      {
        path: 'actors/create',
        loadChildren: () => import('../actors/edit-actors/edit-actors.module').then(m => m.EditActorsModule),
      },
      {
        path: 'actors/:id',
        loadChildren: () => import('../actors/edit-actors/edit-actors.module').then(m => m.EditActorsModule),
      },
      {
        path: 'movies/create',
        loadChildren: () => import('../movies/create-movies/create-movies.module').then(m => m.CreateMoviesModule),
      },
      {
        path: 'movies',
        loadChildren: () => import('../movies/movies-list/movies-list.module').then(m => m.MoviesListModule),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class NavigationRoutingModule {
}
