import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationComponent} from './navigation.component';
import {NzBreadCrumbModule, NzLayoutModule, NzMenuModule} from 'ng-zorro-antd';
import {NavigationRoutingModule} from './navigation.routing.module';

@NgModule({
  declarations: [NavigationComponent],
  imports: [
    CommonModule,
    NavigationRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    NzBreadCrumbModule,
  ]
})
export class NavigationModule {
}
