import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActorModel} from '../models/actor.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActorsService {

  constructor(
    private http: HttpClient,
  ) {
  }

  baseActorsUrl: string = 'http://localhost:3015/api/actors/';

  getAllActors$(): Observable<ActorModel[]> {
    return this.http.get<ActorModel[]>(this.baseActorsUrl);
  }

  getActorById$(id: string): Observable<ActorModel> {
    return this.http.get<ActorModel>(this.baseActorsUrl + id);
  }

  postActor$(data: ActorModel): Observable<ActorModel> {
    return this.http.post<ActorModel>(this.baseActorsUrl, data);
  }

  putActor$(data: ActorModel, id: number) {
    return this.http.put<ActorModel>(this.baseActorsUrl + id, data)
  }

  deleteActorById$(id: number): Observable<Object> {
    return this.http.delete(this.baseActorsUrl + id)
  }

}
