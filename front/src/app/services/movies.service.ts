import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MovieModel} from '../models/movie.model';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(
    private http: HttpClient,
  ) {
  }

  baseMoviesUrl: string = 'http://localhost:3015/api/movies';

  getAllMovies$(): Observable<MovieModel[]> {
    return this.http.get<MovieModel[]>(this.baseMoviesUrl);
  }

  postMovie$(data: MovieModel): Observable<MovieModel> {
    return this.http.post<MovieModel>(this.baseMoviesUrl, data);
  }
}

