import {Timestamp} from 'rxjs';

export interface MovieModel {
  id: number,
  title: string,
  category: string,
  releaseYear: string,
  poster: string,
  directors: string,
  actors: number[],
  synopsis: string,
  rate: number,
  lastViewDate: Timestamp<string>,
  price: number,
}
